package me.mrCookieSlime.slimefun.ancientaltar;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import org.bukkit.inventory.ItemStack;

@Getter
public class AltarRecipe {
	private ItemStack catalyst;
	private List<ItemStack> input;
	private ItemStack output;
	
	public AltarRecipe(List<ItemStack> input, ItemStack output) {
		this.catalyst = input.get(4);
		this.input = new ArrayList<>();
		for (int i = 0; i < input.size(); i++) {
			if (i != 4) this.input.add(input.get(i));
		}
		this.output = output;
		
		Pedestals.recipes.add(this);
	}

}
