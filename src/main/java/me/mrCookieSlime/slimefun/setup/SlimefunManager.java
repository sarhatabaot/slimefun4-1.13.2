package me.mrCookieSlime.slimefun.setup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.potion.PotionEffect;

import me.mrCookieSlime.slimefun.SlimefunStartup;
import me.mrCookieSlime.slimefun.lists.Categories;
import me.mrCookieSlime.slimefun.lists.RecipeType;
import me.mrCookieSlime.slimefun.objects.Category;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.SlimefunArmorPiece;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.SlimefunItem;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.VanillaItem;

public class SlimefunManager {

    public static SlimefunStartup plugin;
    public static String PREFIX;
    public static Map<EntityType, List<ItemStack>> drops = new HashMap<EntityType, List<ItemStack>>();

    public static void registerArmorSet(ItemStack baseComponent, ItemStack[] items, String idSyntax, PotionEffect[][] effects, boolean special, boolean slimefun) {
        String[] components = new String[]{"_HELMET", "_CHESTPLATE", "_LEGGINGS", "_BOOTS"};
        Category cat = special ? Categories.MAGIC_ARMOR : Categories.ARMOR;
        List<ItemStack[]> recipes = new ArrayList<ItemStack[]>();
        recipes.add(new ItemStack[]{baseComponent, baseComponent, baseComponent, baseComponent, null, baseComponent, null, null, null});
        recipes.add(new ItemStack[]{baseComponent, null, baseComponent, baseComponent, baseComponent, baseComponent, baseComponent, baseComponent, baseComponent});
        recipes.add(new ItemStack[]{baseComponent, baseComponent, baseComponent, baseComponent, null, baseComponent, baseComponent, null, baseComponent});
        recipes.add(new ItemStack[]{null, null, null, baseComponent, null, baseComponent, baseComponent, null, baseComponent});
        for (int i = 0; i < 4; i++) {
            if ((effects.length - 1) >= i)
                if (effects[i].length > 0)
                    new SlimefunArmorPiece(cat, items[i], idSyntax + components[i], RecipeType.ARMOR_FORGE, recipes.get(i), effects[i]).register(slimefun);
            else
                new SlimefunItem(cat, items[i], idSyntax + components[i], RecipeType.ARMOR_FORGE, recipes.get(i)).register(slimefun);
        }
    }

    public static void registerArmorSet(ItemStack baseComponent, ItemStack[] items, String idSyntax, boolean slimefun, boolean vanilla) {
        String[] components = new String[]{"_HELMET", "_CHESTPLATE", "_LEGGINGS", "_BOOTS"};
        Category cat = Categories.ARMOR;
        List<ItemStack[]> recipes = new ArrayList<ItemStack[]>();
        recipes.add(new ItemStack[]{baseComponent, baseComponent, baseComponent, baseComponent, null, baseComponent, null, null, null});
        recipes.add(new ItemStack[]{baseComponent, null, baseComponent, baseComponent, baseComponent, baseComponent, baseComponent, baseComponent, baseComponent});
        recipes.add(new ItemStack[]{baseComponent, baseComponent, baseComponent, baseComponent, null, baseComponent, baseComponent, null, baseComponent});
        recipes.add(new ItemStack[]{null, null, null, baseComponent, null, baseComponent, baseComponent, null, baseComponent});
        for (int i = 0; i < 4; i++) {
            if (vanilla) {
                new VanillaItem(cat, items[i], idSyntax + components[i], RecipeType.ARMOR_FORGE, recipes.get(i)).register(slimefun);
            } else {
                new SlimefunItem(cat, items[i], idSyntax + components[i], RecipeType.ARMOR_FORGE, recipes.get(i)).register(slimefun);
            }
        }
    }

    public static boolean isItemSimiliar(ItemStack item, ItemStack SFitem, boolean lore) {
        return isItemSimiliar(item, SFitem, lore, DataType.IF_COLORED);
    }

    public enum DataType {
        ALWAYS,
        NEVER,
        IF_COLORED
    }

    public static boolean isItemSimiliar(ItemStack item, ItemStack SFitem, boolean lore, DataType data) {
        if (item == null) return SFitem == null;
        if (SFitem == null) return false;

        if (!(item.getType() == SFitem.getType() && item.getAmount() >= SFitem.getAmount()))
            return false;

        //ToDo: Removed data_safe - is that correct?
        if (data.equals(DataType.ALWAYS)) {
            if (((Damageable) item.getItemMeta()).getDamage() != ((Damageable) SFitem.getItemMeta()).getDamage()) {
                return false;
            }
        }

        if (item.hasItemMeta() && SFitem.hasItemMeta()) {
            if (item.getItemMeta().hasDisplayName() && SFitem.getItemMeta().hasDisplayName()) {
                if (item.getItemMeta().getDisplayName().equals(SFitem.getItemMeta().getDisplayName())) {
                    if (lore) {
                        if (item.getItemMeta().hasLore() && SFitem.getItemMeta().hasLore()) {
                            return equalsLore(item.getItemMeta().getLore(), SFitem.getItemMeta().getLore());
                        } else return !item.getItemMeta().hasLore() && !SFitem.getItemMeta().hasLore();
                    } else return true;
                } else return false;
            } else if (!item.getItemMeta().hasDisplayName() && !SFitem.getItemMeta().hasDisplayName()) {
                if (lore) {
                    if (item.getItemMeta().hasLore() && SFitem.getItemMeta().hasLore()) {
                        return equalsLore(item.getItemMeta().getLore(), SFitem.getItemMeta().getLore());
                    } else return !item.getItemMeta().hasLore() && !SFitem.getItemMeta().hasLore();
                } else return true;
            } else return false;
        } else return !item.hasItemMeta() && !SFitem.hasItemMeta();

    }

    private static boolean equalsLore(List<String> lore, List<String> lore2) {
        String string1 = "", string2 = "";
        for (String string : lore) {
            if (!string.startsWith("&e&e&7")) string1 = string1 + "-NEW LINE-" + string;
        }
        for (String string : lore2) {
            if (!string.startsWith("&e&e&7")) string2 = string2 + "-NEW LINE-" + string;
        }
        return string1.equals(string2);
    }
}
