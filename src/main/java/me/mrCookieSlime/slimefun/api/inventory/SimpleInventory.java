package me.mrCookieSlime.slimefun.api.inventory;

public interface SimpleInventory {
    /**
     *
     * @return
     */
    String getInventoryTitle();
}
