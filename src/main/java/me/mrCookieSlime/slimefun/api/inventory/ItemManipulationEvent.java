package me.mrCookieSlime.slimefun.api.inventory;

import org.bukkit.inventory.ItemStack;

public interface ItemManipulationEvent {
	
	public ItemStack onEvent(int slot, ItemStack previous, ItemStack next);

}
