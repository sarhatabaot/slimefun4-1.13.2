package me.mrCookieSlime.slimefun.api.energy;

import org.bukkit.block.Block;

public interface EnergyFlowListener {

	void onPulse(Block b);
	
}
