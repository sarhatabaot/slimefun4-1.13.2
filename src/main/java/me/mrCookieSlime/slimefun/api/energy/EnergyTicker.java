package me.mrCookieSlime.slimefun.api.energy;

import me.mrCookieSlime.CSCoreLibPlugin.Configuration.Config;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.SlimefunItem;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.handlers.ItemHandler;

import org.bukkit.Location;

public abstract class EnergyTicker extends ItemHandler {
	
	public abstract double generateEnergy(Location l, SlimefunItem item, Config data);
	public abstract boolean explode(Location l);

	@Override
	public String toCodename() {
		return "EnergyTicker";
	}

}
