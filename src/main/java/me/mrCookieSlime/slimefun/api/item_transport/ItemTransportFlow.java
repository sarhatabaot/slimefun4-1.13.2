package me.mrCookieSlime.slimefun.api.item_transport;

public enum ItemTransportFlow {
	
	INSERT,
	WITHDRAW;

}
