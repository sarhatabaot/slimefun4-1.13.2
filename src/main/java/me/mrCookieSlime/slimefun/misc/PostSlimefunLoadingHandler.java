package me.mrCookieSlime.slimefun.misc;

import java.util.List;

import me.mrCookieSlime.slimefun.objects.SlimefunItem.SlimefunItem;

@FunctionalInterface
public interface PostSlimefunLoadingHandler {
	
	void run(List<SlimefunItem> preloaded, List<SlimefunItem> loaded, List<SlimefunItem> postloaded);

}
