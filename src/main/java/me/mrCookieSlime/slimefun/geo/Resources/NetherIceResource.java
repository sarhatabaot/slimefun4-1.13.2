package me.mrCookieSlime.slimefun.geo.Resources;

import org.bukkit.block.Biome;
import org.bukkit.inventory.ItemStack;

import me.mrCookieSlime.slimefun.geo.OreGenResource;
import me.mrCookieSlime.slimefun.lists.SlimefunItems;

public class NetherIceResource implements OreGenResource {
	
	@Override
	public int getDefaultSupply(Biome biome) {
		if (biome == Biome.NETHER) {
			return 32;
		}
		return 0;
	}

	@Override
	public String getName() {
		return "Nether Ice";
	}

	@Override
	public ItemStack getIcon() {
		return SlimefunItems.NETHER_ICE.clone();
	}

	@Override
	public String getMeasurementUnit() {
		return "Blocks";
	}

}
