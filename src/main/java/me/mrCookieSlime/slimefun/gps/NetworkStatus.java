package me.mrCookieSlime.slimefun.gps;

public enum NetworkStatus {
	ONLINE,
	OFFLINE
}
