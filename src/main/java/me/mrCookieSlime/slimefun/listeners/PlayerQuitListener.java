package me.mrCookieSlime.slimefun.listeners;

import me.mrCookieSlime.slimefun.api.PlayerProfile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import me.mrCookieSlime.slimefun.guide.SlimefunGuide;
import me.mrCookieSlime.slimefun.SlimefunStartup;

public class PlayerQuitListener implements Listener {

	public PlayerQuitListener(SlimefunStartup plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onDisconnect(PlayerQuitEvent e) {
		SlimefunGuide.history.remove(e.getPlayer().getUniqueId());
		if (PlayerProfile.isLoaded(e.getPlayer().getUniqueId())) {
			PlayerProfile.fromUUID(e.getPlayer().getUniqueId()).markForDeletion();
		}
	}

}
