package me.mrCookieSlime.slimefun.listeners;

import me.mrCookieSlime.slimefun.SlimefunStartup;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.JetBoots;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.Jetpack;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.SlimefunItem;
import me.mrCookieSlime.slimefun.objects.tasks.JetBootsTask;
import me.mrCookieSlime.slimefun.objects.tasks.JetpackTask;
import me.mrCookieSlime.slimefun.objects.tasks.MagnetTask;
import me.mrCookieSlime.slimefun.objects.tasks.ParachuteTask;
import me.mrCookieSlime.slimefun.api.Slimefun;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class GearListener implements Listener {
	
	public GearListener(SlimefunStartup plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onToggleSneak(PlayerToggleSneakEvent e) {
		if (e.isSneaking()) {
			final Player p = e.getPlayer();
			if (SlimefunItem.getByItem(p.getInventory().getChestplate()) != null) {
				SlimefunItem item = SlimefunItem.getByItem(p.getInventory().getChestplate());
				if (item instanceof Jetpack) {
					if (Slimefun.hasUnlocked(p, item.getItem(), true)) {
						double thrust = ((Jetpack) item).getThrust();
						if (thrust > 0.2) {
							JetpackTask task = new JetpackTask(p, thrust);
							task.setID(Bukkit.getScheduler().scheduleSyncRepeatingTask(SlimefunStartup.instance, task, 0L, 3L));
						}
					}
				}
				else if (item.isItem(SlimefunItem.getItem("PARACHUTE"))) {
					if (Slimefun.hasUnlocked(p, SlimefunItem.getItem("PARACHUTE"), true)) {
						ParachuteTask task = new ParachuteTask(p);
						task.setID(Bukkit.getScheduler().scheduleSyncRepeatingTask(SlimefunStartup.instance, task, 0L, 3L));
					}
				}
			}
			if (SlimefunItem.getByItem(p.getInventory().getBoots()) != null) {
				SlimefunItem item = SlimefunItem.getByItem(p.getInventory().getBoots());
				if (item instanceof JetBoots) {
					if (Slimefun.hasUnlocked(p, item.getItem(), true)) {
						double speed = ((JetBoots) item).getSpeed();
						if (speed > 0.2) {
							JetBootsTask task = new JetBootsTask(p, speed);
							task.setID(Bukkit.getScheduler().scheduleSyncRepeatingTask(SlimefunStartup.instance, task, 0L, 2L));
						}
					}
				}
			}
			if (p.getInventory().containsAtLeast(SlimefunItem.getItem("INFUSED_MAGNET"), 1)) {
				MagnetTask task = new MagnetTask(p);
				task.setID(Bukkit.getScheduler().scheduleSyncRepeatingTask(SlimefunStartup.instance, task, 0L, 8L));
			}
		}
	}

}
