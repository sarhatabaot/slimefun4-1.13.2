package me.mrCookieSlime.slimefun.objects;

import me.mrCookieSlime.slimefun.objects.SlimefunItem.SlimefunItem;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.UnregisterReason;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public interface SlimefunBlockHandler {
	
	void onPlace(Player p, Block b, SlimefunItem item);

	boolean onBreak(Player p, Block b, SlimefunItem item, UnregisterReason reason);
}
