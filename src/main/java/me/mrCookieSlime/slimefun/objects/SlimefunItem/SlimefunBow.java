package me.mrCookieSlime.slimefun.objects.SlimefunItem;

import me.mrCookieSlime.slimefun.lists.Categories;
import me.mrCookieSlime.slimefun.lists.RecipeType;

import org.bukkit.inventory.ItemStack;

public class SlimefunBow extends SlimefunItem {

	public SlimefunBow(ItemStack item, String id, ItemStack[] recipe) {
		super(Categories.WEAPONS, item, id, RecipeType.MAGIC_WORKBENCH, recipe);
	}

}
