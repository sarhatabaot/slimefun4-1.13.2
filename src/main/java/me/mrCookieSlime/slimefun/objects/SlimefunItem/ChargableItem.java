package me.mrCookieSlime.slimefun.objects.SlimefunItem;

import me.mrCookieSlime.slimefun.lists.RecipeType;
import me.mrCookieSlime.slimefun.objects.Category;

import org.bukkit.inventory.ItemStack;

public class ChargableItem extends SlimefunItem {
	
	String chargeType;

	public ChargableItem(Category category, ItemStack item, String id, RecipeType recipeType, ItemStack[] recipe, String chargeType) {
		super(category, item, id, recipeType, recipe);
		this.chargeType = chargeType;
	}
	
	public ChargableItem(Category category, ItemStack item, String id, RecipeType recipeType, ItemStack[] recipe, String chargeType, String[] keys, Object[] values) {
		super(category, item, id, recipeType, recipe, keys, values);
		this.chargeType = chargeType;
	}
	
	public String getChargeType()		{		return this.chargeType;		}

}
