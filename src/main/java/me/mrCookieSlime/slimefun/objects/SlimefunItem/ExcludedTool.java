package me.mrCookieSlime.slimefun.objects.SlimefunItem;

import org.bukkit.inventory.ItemStack;

import me.mrCookieSlime.slimefun.lists.RecipeType;
import me.mrCookieSlime.slimefun.objects.Category;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.Interfaces.NotPlaceable;

public class ExcludedTool extends SlimefunItem implements NotPlaceable {

	public ExcludedTool(Category category, ItemStack item, String id,RecipeType recipeType, ItemStack[] recipe) {
		super(category, item, id, recipeType, recipe);
	}

}
