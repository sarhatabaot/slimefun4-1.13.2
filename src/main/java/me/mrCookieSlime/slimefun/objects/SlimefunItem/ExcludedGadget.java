package me.mrCookieSlime.slimefun.objects.SlimefunItem;

import org.bukkit.inventory.ItemStack;

import me.mrCookieSlime.slimefun.lists.RecipeType;
import me.mrCookieSlime.slimefun.objects.Category;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.Interfaces.NotPlaceable;

public class ExcludedGadget extends SlimefunGadget implements NotPlaceable {

	public ExcludedGadget(Category category, ItemStack item, String id, RecipeType recipeType, ItemStack[] recipe, ItemStack[] machineRecipes) {
		super(category, item, id, recipeType, recipe, machineRecipes);
	}
	
	public ExcludedGadget(Category category, ItemStack item, String id, RecipeType recipeType, ItemStack[] recipe, ItemStack[] machineRecipes, String[] keys, Object[] values) {
		super(category, item, id, recipeType, recipe, machineRecipes, keys, values);
	}
}
