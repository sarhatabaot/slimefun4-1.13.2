package me.mrCookieSlime.slimefun.objects.SlimefunItem;

import me.mrCookieSlime.slimefun.lists.RecipeType;
import me.mrCookieSlime.slimefun.objects.Category;

import org.bukkit.inventory.ItemStack;

public class Juice extends SlimefunItem {

	public Juice(Category category, ItemStack item, String id, RecipeType recipeType, ItemStack[] recipe) {
		super(category, item, id, recipeType, recipe);
	}

}
