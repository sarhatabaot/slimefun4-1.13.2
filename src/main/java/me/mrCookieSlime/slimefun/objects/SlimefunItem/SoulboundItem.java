package me.mrCookieSlime.slimefun.objects.SlimefunItem;

import me.mrCookieSlime.slimefun.lists.RecipeType;
import me.mrCookieSlime.slimefun.objects.Category;

import org.bukkit.inventory.ItemStack;

public class SoulboundItem extends SlimefunItem {

	public SoulboundItem(Category category, ItemStack item, String id, ItemStack[] recipe) {
		super(category, item, id, RecipeType.MAGIC_WORKBENCH, recipe);
	}
	public SoulboundItem(Category category, ItemStack item, String id, RecipeType type, ItemStack[] recipe) {
		super(category, item, id, type, recipe);
	}

}
