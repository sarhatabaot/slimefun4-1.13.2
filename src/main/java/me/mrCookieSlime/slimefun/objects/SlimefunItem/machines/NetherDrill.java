package me.mrCookieSlime.slimefun.objects.SlimefunItem.machines;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.mrCookieSlime.slimefun.geo.OreGenResource;
import me.mrCookieSlime.slimefun.geo.OreGenSystem;
import me.mrCookieSlime.slimefun.lists.RecipeType;
import me.mrCookieSlime.slimefun.lists.SlimefunItems;
import me.mrCookieSlime.slimefun.objects.Category;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.abstractItems.ADrill;

public abstract class NetherDrill extends ADrill {

	public NetherDrill(Category category, ItemStack item, String name, RecipeType recipeType, ItemStack[] recipe) {
		super(category, item, name, recipeType, recipe);
	}

	@Override
	public OreGenResource getOreGenResource() {
		return OreGenSystem.getResource("Nether Ice");
	}

	@Override
	public ItemStack[] getOutputItems() {
		return new ItemStack[] {SlimefunItems.NETHER_ICE};
	}

	@Override
	public int getProcessingTime() {
		return 24;
	}

	@Override
	public String getInventoryTitle() {
		return "&4Nether Drill";
	}

	@Override
	public ItemStack getProgressBar() {
		return new ItemStack(Material.DIAMOND_PICKAXE);
	}

	@Override
	public String getMachineIdentifier() {
		return "NETHER_DRILL";
	}
	
}
