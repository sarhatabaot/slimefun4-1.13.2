package me.mrCookieSlime.slimefun.objects.SlimefunItem;

import org.bukkit.inventory.ItemStack;

import me.mrCookieSlime.slimefun.lists.RecipeType;
import me.mrCookieSlime.slimefun.objects.Category;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.Interfaces.NotPlaceable;

public class ExcludedSoulboundTool extends SoulboundItem implements NotPlaceable {

	public ExcludedSoulboundTool(Category category, ItemStack item, String id, RecipeType type, ItemStack[] recipe) {
		super(category, item, id, type, recipe);
	}
	
	public ExcludedSoulboundTool(Category category, ItemStack item, String id, ItemStack[] recipe) {
		super(category, item, id, recipe);
	}

}
