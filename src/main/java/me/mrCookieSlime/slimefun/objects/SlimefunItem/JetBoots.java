package me.mrCookieSlime.slimefun.objects.SlimefunItem;

import me.mrCookieSlime.slimefun.lists.Categories;
import me.mrCookieSlime.slimefun.lists.RecipeType;

import org.bukkit.inventory.ItemStack;

public class JetBoots extends DamagableChargableItem {
	
	double speed;

	public JetBoots(ItemStack item, String id, ItemStack[] recipe, double speed) {
		super(Categories.TECH, item, id, RecipeType.ENHANCED_CRAFTING_TABLE, recipe, "Jet Boots");
		this.speed = speed;
	}
	
	public double getSpeed() {
		return speed;
	}
	

}
