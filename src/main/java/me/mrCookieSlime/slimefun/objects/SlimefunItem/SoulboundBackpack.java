package me.mrCookieSlime.slimefun.objects.SlimefunItem;

import me.mrCookieSlime.slimefun.lists.RecipeType;
import me.mrCookieSlime.slimefun.objects.Category;

import org.bukkit.inventory.ItemStack;

public class SoulboundBackpack extends SlimefunBackpack {

	public SoulboundBackpack(int size, Category category, ItemStack item, String id, ItemStack[] recipe) {
		super(size, category, item, id, RecipeType.MAGIC_WORKBENCH, recipe);
	}
	public SoulboundBackpack(int size, Category category, ItemStack item, String id, RecipeType type, ItemStack[] recipe) {
		super(size, category, item, id, type, recipe);
	}

}
