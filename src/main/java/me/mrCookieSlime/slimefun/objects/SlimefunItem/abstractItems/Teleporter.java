package me.mrCookieSlime.slimefun.objects.SlimefunItem.abstractItems;

import me.mrCookieSlime.slimefun.lists.RecipeType;
import me.mrCookieSlime.slimefun.objects.Category;
import me.mrCookieSlime.slimefun.objects.SlimefunBlockHandler;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.SlimefunItem;
import me.mrCookieSlime.slimefun.objects.SlimefunItem.UnregisterReason;
import me.mrCookieSlime.slimefun.api.BlockStorage;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class Teleporter extends SlimefunItem {

	public Teleporter(Category category, ItemStack item, String id, RecipeType recipeType, ItemStack[] recipe) {
		super(category, item, id, recipeType, recipe);
		
		SlimefunItem.registerBlockHandler(id, new SlimefunBlockHandler() {
			
			@Override
			public void onPlace(Player p, Block b, SlimefunItem item) {
				BlockStorage.addBlockInfo(b, "owner", p.getUniqueId().toString());
			}
			
			@Override
			public boolean onBreak(Player p, Block b, SlimefunItem item, UnregisterReason reason) {
				return true;
			}
		});
	}
	
	public abstract void onInteract(Player p, Block b) throws Exception;

}
